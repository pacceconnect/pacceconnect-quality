﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace EndToEndTests.Core;

public class DSL : GlobalVariables
{
    public static void Wait(int time) => Thread.Sleep(time);
    
    public void WriteText(string xpath, string value)
    {
        driver.FindElement(By.XPath(xpath)).SendKeys(value);
    }
    
    public void Click(string xpath)
    {
        driver.FindElement(By.XPath(xpath)).Click();
    }
    
    public void VerifyData(string xpath, string value)
    {
        Assert.That(driver.FindElement(By.XPath(xpath)).Text, Is.EqualTo(value));
    }

    public void VerifyAlertMessage(string value)
    {
        var await = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        var alert = await.Until(ExpectedConditions.AlertIsPresent());

        Assert.That(alert.Text, Is.EqualTo(value));
        alert.Accept();
    }
}