﻿using OpenQA.Selenium;

namespace EndToEndTests.Core;

public class GlobalVariables
{
    public IWebDriver driver;
    public bool isDriverQuite = true;
    public bool headlessTest = false;
}