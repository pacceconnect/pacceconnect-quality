﻿using NUnit.Framework;
using OpenQA.Selenium.Edge;

namespace EndToEndTests.Core
{
    public class Start : DSL
    {
        private void ConfigureBrowser()
        {
            var headlessMode = new EdgeOptions();
            headlessMode.AddArgument("window-size=1920,1080");
            headlessMode.AddArgument("disk-cache-size=0");
            headlessMode.AddArgument("headless");
            
            var devMode = new EdgeOptions();
            devMode.AddArgument("start-maximized");
            devMode.AddArgument("disk-cache-size=0");

            if (headlessTest)
            {
                driver = new EdgeDriver(headlessMode);
            }
            else
            {
                driver = new EdgeDriver(devMode);
                isDriverQuite = false;
            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }
    
        [SetUp]
        public void Setup()
        {
            ConfigureBrowser();
            driver.Navigate().GoToUrl("https://musical-dieffenbachia-115c2b.netlify.app");
        }
        
        [TearDown]
        public void TearDown()
        {
            if(isDriverQuite) driver.Quit();
        }
    }
}