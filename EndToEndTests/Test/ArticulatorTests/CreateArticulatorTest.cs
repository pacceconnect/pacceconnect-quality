﻿using EndToEndTests.Page.ArticulatorPage;
using NUnit.Framework;

namespace EndToEndTests.Test.ArticulatorTests;

public class CreateArticulatorTest : CreateArticulatorPage
{
    [Test]
    public void ValidateCreateArticulator()
    {
        ClickToLogin();
        ClickToRegister();
        ClickToArticulatorCard();
        ClickToContinue();
        WriteFirstName();
        WriteSurName();
        WriteMatriculation();
        SelectCourse();
        WriteUserName();
        WriteCellPhone();
        WriteEmail();
        WritePassword();
        WritePasswordConfirmation();
        ClickBtnRegister();
        ValidateCadaster();
    }

    [Test]
    public void ValidateCreateArticulatorAndLoginWithUserName()
    {
        ClickToLogin();
        ClickToRegister();
        ClickToArticulatorCard();
        ClickToContinue();
        WriteFirstName();
        WriteSurName();
        WriteMatriculation();
        SelectCourse();
        WriteUserName();
        WriteCellPhone();
        WriteEmail();
        WritePassword();
        WritePasswordConfirmation();
        ClickBtnRegister();
        ValidateCadaster();
        WriteUserNameLogin();
        WritePasswordLogin();
        ClickBtnLogin();
        ValidateLogin();
    }
    
    [Test]
    public void ValidateCreateArticulatorAndLoginWithEmail()
    {
        ClickToLogin();
        ClickToRegister();
        ClickToArticulatorCard();
        ClickToContinue();
        WriteFirstName();
        WriteSurName();
        WriteMatriculation();
        SelectCourse();
        WriteUserName();
        WriteCellPhone();
        WriteEmail();
        WritePassword();
        WritePasswordConfirmation();
        ClickBtnRegister();
        ValidateCadaster();
        WirteEmailLogin();
        WritePasswordLogin();
        ClickBtnLogin();
        ValidateLogin();
    }
}