﻿using EndToEndTests.Page.ManagerPage;
using NUnit.Framework;

namespace EndToEndTests.Test.ManagerTests;

public class CreateManager : CreateManagerPage
{
    [Test]
    public void ValidateCreateManager()
    {
        ClickToLogin();
        ClickToRegister();
        ClickToManagerCard();
        ClickToContinue();
        WriteFullName();
        WriteUserName();
        WriteCellPhone();
        WriteEmail();
        WritePassword();
        WritePasswordConfirmation();
        ClickBtnRegister();
        ValidateCadaster();
    }
    
    [Test]
    public void ValidateCreateManagerAndLoginWithUserName()
    {
        ClickToLogin();
        ClickToRegister();
        ClickToManagerCard();
        ClickToContinue();
        WriteFullName();
        WriteUserName();
        WriteCellPhone();
        WriteEmail();
        WritePassword();
        WritePasswordConfirmation();
        ClickBtnRegister();
        ValidateCadaster();
        WriteUserNameLogin();
        WritePasswordLogin();
        ClickBtnLogin();
        ValidateLogin();
    }
    
    [Test]
    public void ValidateCreateManagerAndLoginWithEmail()
    {
        ClickToLogin();
        ClickToRegister();
        ClickToManagerCard();
        ClickToContinue();
        WriteFullName();
        WriteUserName();
        WriteCellPhone();
        WriteEmail();
        WritePassword();
        WritePasswordConfirmation();
        ClickBtnRegister();
        ValidateCadaster();
        WirteEmailLogin();
        WritePasswordLogin();
        ClickBtnLogin();
        ValidateLogin();
    }
}