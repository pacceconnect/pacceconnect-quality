﻿using EndToEndTests.Core;

namespace EndToEndTests.Page.ManagerPage;

public class CreateManagerPage : Start
{
    private string userName = $"test_user_{Guid.NewGuid()}";
    private string email = $"test_email_{Guid.NewGuid()}@test.com";
    public void ClickToLogin()
    {
        Click("/html/body/app-root/app-wecolme-page/mat-grid-list/div/mat-grid-tile-header/app-welcome-header/div/div/div[2]/div[3]/a/button");
    }

    public void ClickToRegister()
    {
        Click("/html/body/app-root/app-login-page/mat-grid-list/div/mat-grid-tile[1]/div/div/div/app-login-main/div[2]/button");
    }

    public void ClickToManagerCard()
    {
        Click("/html/body/app-root/app-login-page/mat-grid-list/div/mat-grid-tile[1]/div/div/div/app-register-choice-role/div/div[2]/div[1]/div");
    }

    public void ClickToContinue()
    {
        Click("/html/body/app-root/app-login-page/mat-grid-list/div/mat-grid-tile[1]/div/div/div/app-register-choice-role/div/div[3]/button");
    }
    
    public void WriteFullName()
    {
        WriteText("//*[@id='mat-input-2']", "firs_test_full_name");
    }
    
    public void WriteUserName()
    {
        WriteText("//*[@id='mat-input-3']", userName);
    }
    
    public void WriteCellPhone()
    {
        WriteText("//*[@id='mat-input-4']", "123456789101");
    }
    
    public void WriteEmail()
    {
        WriteText("//*[@id='mat-input-5']", email);
    }
    
    public void WritePassword()
    {
        WriteText("//*[@id='mat-input-6']", "12345678");
    }
    
    public void WritePasswordConfirmation()
    {
        WriteText("//*[@id='mat-input-7']", "12345678");
    }
    
    public void ClickBtnRegister()
    {
        Click("/html/body/app-root/app-login-page/mat-grid-list/div/mat-grid-tile[1]/div/div/div/app-register-manager-form/div[1]/form/div[2]/button");
    }

    public void ValidateCadaster()
    {
        VerifyAlertMessage("Cadastro realizado com sucesso! Faça login com suas credenciais");
    }
    
    public void WriteUserNameLogin()
    {
        WriteText("//*[@id='mat-input-8']", userName);
    }
    
    public void WirteEmailLogin()
    {
        WriteText("//*[@id='mat-input-8']", email);
    }
    
    public void WritePasswordLogin()
    {
        WriteText("//*[@id='mat-input-9']", "12345678");
    }
    
    public void ClickBtnLogin()
    {
        Click("/html/body/app-root/app-login-page/mat-grid-list/div/mat-grid-tile[1]/div/div/div/app-login-main/div[1]/form/div[2]/div/button");
    }
    
    public void ValidateLogin()
    {
        VerifyAlertMessage($"Login realizado com sucesso {userName}");
    }
}