using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using tests_backend.Models;

namespace tests_backend.ArticulatorTests;

public class Tests
{
    private const string uri = "https://tender-sofa-production.up.railway.app/";
    private RestClient client;
    private RestRequest endpoint;
    private IRestResponse resp;
    private string randomUsername = string.Empty;
    private string randomEmail = string.Empty;
    public int id;
    public string token = String.Empty;
    public string role = String.Empty;
    
    [SetUp] public void Setup()
    {
        client = Client(uri);
        randomUsername = $"test{Guid.NewGuid()}";
        randomEmail = $"test{Guid.NewGuid()}@test.com";
    }
    
    public RestClient Client(string uri)
    {
        client = new RestClient(uri);
        return client;
    }
    
    public RestRequest Endpoint(string route)
    {
        endpoint = new RestRequest(route);
        return endpoint;
    }

    public void Get()
    {
        endpoint.Method = Method.GET;
        endpoint.RequestFormat = DataFormat.Json;
    }
    
    public void Post()
    {
        endpoint.Method = Method.POST;
        endpoint.RequestFormat = DataFormat.Json;
    }

    public void Body_json(string _body)
    {
        endpoint.AddParameter("application/json", _body, ParameterType.RequestBody);
    }
    
    public void InsertToken(string _token)
    {
        endpoint.AddHeader("Authorization", $"Bearer {_token}");
    }

    public IRestResponse StatusCode(int code)
    {
       resp = client.Execute(endpoint);

        if (resp.IsSuccessful)
        {
            var status = (int)resp.StatusCode;
            Assert.AreEqual(code, status);
        }
        else
        {
            var status = (int)resp.StatusCode;
            var desc = resp.StatusDescription;
            var content = resp.Content;

            Console.WriteLine($"{status} - {desc}");
            Console.WriteLine(content);
            Assert.AreEqual(code, status);
        }

        return resp;
    }

    public void ReturnText()
    {
        var obs = resp.Content;
        Console.WriteLine(obs);
    }

    public string _json_cadaster()
    {
        var body = $@"
        {{
            ""name"": ""first_test"",
            ""surName"": ""last_test"",
            ""userName"": ""{randomUsername}"",
            ""email"": ""{randomEmail}"",
            ""password"": ""12345678"",
            ""course"": 1,
            ""matriculation"": 11111111
        }}";

        return body;
    }

    public string _json_auhtenticate(string type)
    {
        var body = string.Empty;
        
        if (type == "userName")
        {
            body = $@"
            {{
                ""emailOrUserName"": ""{randomUsername}"",
                ""password"": ""12345678""
            }}";
            
            return body;
        }
        
        body = $@"
        {{
            ""emailOrUserName"": ""{randomEmail}"",
            ""password"": ""12345678""
        }}";

        return body;
    }

    public dynamic GetValue(dynamic key)
    {
        dynamic obj = JProperty.Parse(resp.Content);

        var value = obj[key];
        return value;
    }
    
    public void Cadaster()
    {
        Endpoint("articulator/signup");
        Post();
        Body_json(_json_cadaster());
        StatusCode(201);
        ReturnText();
        
        id = GetValue("id");
        Assert.IsNotNull(id);
    }
    
    public void CadasterWithTheSameInformation()
    {
        Endpoint("/articulator/signup");
        Post();
        Body_json(_json_cadaster());
        StatusCode(400);

        var errorsConverted = JsonConvert.DeserializeObject<ErrorResponse>(resp.Content);
        
        Assert.AreEqual("Email already exists", errorsConverted.errors.Message.First());
        Assert.AreEqual("ARTICULATOR_EMAIL_ALREADY_EXISTS", errorsConverted.errors.ErrorCode.First());
    }

    public void AuthenticateWithUserName()
    {
        Endpoint("Auth/authenticate");
        Post();
        Body_json(_json_auhtenticate("userName"));
        StatusCode(200);
        ReturnText();

        token = GetValue("token");
        Assert.IsNotNull(token);
        
        role = GetValue("role");
        Assert.AreEqual("Articulator", role);
    }
    
    public void AuthenticateWithEmail()
    {
        Endpoint("Auth/authenticate");
        Post();
        Body_json(_json_auhtenticate("email"));
        StatusCode(200);
        ReturnText();

        token = GetValue("token");
        Assert.IsNotNull(token);
        
        role = GetValue("role");
        Assert.AreEqual("Articulator", role);
    }

    public void GetById()
    {
        Endpoint($"articulator/{id}");
        Get();
        InsertToken(token);
        StatusCode(200);
        ReturnText();
        
        string username_get = GetValue("userName");
        Assert.IsNotNull(username_get);
        Assert.AreEqual(randomUsername, username_get);
        
        string email_get = GetValue("email");
        Assert.IsNotNull(email_get);
        Assert.AreEqual(randomEmail, email_get);
    }

    [Test]
    public void CadasterNewArticulatorUserAndLogin()
    {
        Cadaster();
        AuthenticateWithUserName();
        AuthenticateWithEmail();
        GetById();
    }
    
    [Test]
    public void CadasterNewManagerUserAndCadasterOtherWithTheSameInformations()
    {
        Cadaster();
        CadasterWithTheSameInformation();
    }
}