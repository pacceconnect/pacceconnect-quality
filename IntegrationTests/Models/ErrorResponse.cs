﻿namespace tests_backend.Models;

public class ErrorResponse
{
    public string title { get; set; }
    public int status { get; set; }
    public Errors errors { get; set; }
}

public class Errors
{
    public string[] Message { get; set; }
    public string[] ErrorCode { get; set; }
}